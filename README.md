# Screen blanking + locking or lidclose + blanking for Mabox Linux. 

## This page has 2 parts. Each with their own function. 

- (part 1) when blanking screen, lock screen.

- (part 2) lid close lock screen. (laptop lid)

_Method: without the need of xfconf. (xfce4-powermanager{disable/remove})_



## (part 1): Check screen blanking and lock the screen.



##### Lock screen when screen goes blank. 
For that `xss-lock` is needed to be installed. (man xss-lock)

```
yay xss-lock
```



The following script  need to be started during boot.

It checks every sec the dpms status. *(Openbox startup)*

#### Script discription: When screen goes blank (dpms on), betterlockscreen (i3lock) will start.

Check path of both applications with the following commands.

`which xss-lock`

`which betterlockscreen`

Use those path's instead for the script. 

screenlockscript.sh
```
#!/bin/bash

watch -n 1 /usr/bin/xss-lock -- /usr/bin/betterlockscreen --lock
```

Or another way...

screenlockscript.sh
```
#!/bin/bash

while true; do
	xss-lock -- betterlockscreen --lock 
  # Sleep for 1 second
  sleep 1
done
```

Edit Autostart openbox to add script to the list.
```
geany $HOME/.config/openbox/autostart
```
Put this line halfway the list. (not at the end)
```
(screenlockscript.sh) &
```


Make executable if not already done.
```
chmod +x screenlockscript.sh
```

In the case the file is not yet in the bin. 

Copy the file to home bin dir with...
```
cp screenlockscript.sh ~/bin
```

From now on the script will run from boot.


# (part 2): lid closed blanking and lockscreen on. 
# [Follow step by step. Don't change path or names.] 

### The Only change one can make is,  
### change `betterlockscreen` for your desired screen-locker like `i3lock`.

Note: Line of interest. 

`su -l <USER> -c "export DISPLAY=:0 && xset dpms force off && exec /usr/bin/betterlockscreen --lock"`

**Discription : When lid is closed display goes off and betterlockscreen goes on.**

#### How to achieve this in 6 steps. (need root acces with sudo)

1. **Install `acpid`** if it's not already installed. `acpid` is a daemon that handles ACPI events. You can install it using the following command:

   ```
   sudo pacman -S acpid
   ```

2. **Enable and start the 'acpid' service**:

   ```
   sudo systemctl enable acpid
   sudo systemctl start acpid
   sudo systemctl status acpid
   ```

3. **Create an ACPI event script** to run a command when the lid is closed. Open a text editor (e.g., 'nano' or 'vim') to create a custom ACPI event script:

```
   sudo nano /etc/acpi/events/lid
```

   Add the following lines to the file:

```
   event=button/lid.*
   action=/etc/acpi/lid.sh %e
```

   This configuration tells 'acpid' to run the '/etc/acpi/lid.sh' script when the lid button is pressed.

4. **Create the `lid.sh` script**:

   Create the script that will turn off the display when the lid is closed:

```
   sudo nano /etc/acpi/lid.sh
```

   Add the following lines to the script:
   
   Change now your command like `betterlockscreen` or `i3lock`. Full path to application.
    
    

```
   #!/bin/sh

case "$1" in
    button/lid)
        case "$3" in
            close)
                # When the lid is closed, turn off the display and lock the screen
                su -l <USER> -c "export DISPLAY=:0 && xset dpms force off && exec /usr/bin/betterlockscreen --lock"
                ;;
            open)
                # When the lid is opened, turn on the display
                su -l <USER> -c "export DISPLAY=:0 && xset dpms force on"
                ;;
        esac
        ;;
esac

```

   Make also sure to replace `<USER>` with your actual username.
   
   To get username:
   ```
   echo "$USER"
   ```
   

   
5. **Make the script executable**:

   ```bash
   sudo chmod +x /etc/acpi/lid.sh
   ```

6. **Reload the 'acpid' service**:

   ```
   sudo systemctl restart acpid
   ```

Now, when you close the laptop lid, the display should turn off, and when you open the lid, the display should turn back on.


